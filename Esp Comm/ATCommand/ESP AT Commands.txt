# Separate command and description by tab or at least two space
# $(Variable Name)[Type]	

# $(String)[s]
# $(Name, val1=Choice1|val2=Choice2)[c] 
# $(Name, val1|val2=Choice2)[c] 
# $(Integer)[i] 
# $(Decimal)[d]
# $(Bytes)[b]

# $(Url)[u]
# $(Filepath)[f]

AT					General test, send "AT" and you should receive "AT" back as a reply.
AT+RST			Restart the esp module (return [Rr]eady)	
AT+GMR			Get current firmware version

AT+CWMODE=$(Wifi Mode, 1=Station*|2=AP|3=Station & AP)[c]		Set wifi mode 1=station mode, 2=AP, 3=both, Sta is the default mode of router, AP is a normal mode for devices 
AT+CWMODE?		Get current wifi mode
AT+CWMODE=?		Get which wifi mode is available

AT+CWJAP="$(Wifi Name/SSID)","$(Wifi Password)"		Join the wifi AP/router
AT+CWJAP?	Verify connection to the AP

AT+CWLAP		List the AP
AT+CWQAP		Quit the AP

AT+CWLIF			Current IP address
AT+CIPSTATUS	Get the connection status

AT+CIPMUX=$(Mode, 0=Single Connection|1=Multiple Connection)[c] 				Set connection mode; single or multiple connection. 

AT+CIPSTART="$(Type, TCP=TCP/IP|UDP)[c]","$(Address)",$(Port)[i]								Open single connection. Must run AT+CIPMUX=0 first. 
AT+CIPSTART=$(ID, 1|2|3|4)[c], "$(Type, TCP=TCP/IP|UDP)[c]","$(Address)",$(Port)[i]		Open multiple connection. Must run AT+CIPMUX=1 first. 

AT+CIPSEND=$(ID, 1|2|3|4)[c], $(Length)[i] 	Multiple connection (+CIPMUX=1) eg. send data: AT+CIPSEND=4,15 and then enter the data. 
AT+CIPSEND=$(Length)[i]		Single connection(+CIPMUX=0) AT+CIPSEND=<length>
AT+CIPSEND=?

AT+CIPCLOSE				Close Single TCP or UDP connection	 	
AT+CIPCLOSE=$(ID, 1|2|3|4)[c]		Close Multiple TCP or UDP connection #id	
AT+CIPCLOSE=?

AT+CIFSR		Get IP address TCP/IP
AT+CIPMODE=$(Data Mode, 0=Normal|1=Transparent Transmission*)[c]		Set transfer mode  (1 is default) 

AT+CWSAP="$(SSID)","$(Password)",$(Channel, 1|2|3|4|5|6|7|8|9|10|11|12|13|14)[c],$(Encryption, 0=Open|1=WEP|2=WPA_PSK|3=WPA2_PSK|4=WPA_WPA2_PSK)[c]		Setup ESP as standalone an AP http://goo.gl/4mmrTG
AT+CWSAP? 		Check the current ESP AP

AT+CIPSERVER=$(Mode, 1=Open/Setup Server|0=Close Server)[c]		Set as TCP/IP server, no port (ie. listen to all incoming ports)
AT+CIPSERVER=$(Mode, 1=Open/Setup Server|0=Close Server)[c], $(Port)[d]		Set as TCP/IP server

AT+CIPSTO=$(Timeout, 1-28800)[i]		Set the socket time out (in seconds)
AT+CIPSTO=?		Get current timeout

AT+CIOBAUD=$(Baud Rate, 9600|19200|38400|57600|74880|115200|230400|460800|921600)[c]		Set baud rate
AT+CIOBAUD?  				Get current baud rate 

AT+CSYSWDTENABLE	Enable watchdog, auto restart when program have errors occurred
AT+CSYSWDTDISABLE 	Disable watchdog, auto restart when program have errors occurred

AT+CIUPDATE		Run cloud update.

Script+GET=$(Url), $(Header), $(Cookie)
Script+POST=$(Url), $(Data), $(Header), $(Cookie)
Script+POSTFILE=$(Url), $(Data), $(File)[f], $(Header), $(Cookie)







