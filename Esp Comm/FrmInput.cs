﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using Esp_Comm.DataTypes;
using LittleUmph;
using LittleUmph.GUI.Components;

namespace Esp_Comm
{
    public partial class FrmInput : Form
    {
        string _groupName;
        DataStore _storage;

        #region [ Constructors ]
        public FrmInput(SettingSaver settingSaver, string template)
        {
            _groupName = Str.MaxLength(Str.SplitIndex(template.Replace("+", "-"), "=", 0), 20);
            _storage = settingSaver.DataStore;

            InitializeComponent();

            lblTemplate.Text = template;
            ParseTemplate(template);

            //_storage.Set(_groupName, "Test", "Here");
        }
        #endregion

        #region [ Parse Template & Create Data Fields ]
        /// <summary>
        /// Parses the template and create the data fields.
        /// </summary>
        /// <param name="template">The template.</param>
        private void ParseTemplate(string template)
        {
            var atRegex = new Regex(@"\$\((?<name>.+?)(?<choice>, .+?)?\)(\s*\[(?<type>\w)\])?");
            MatchCollection matches = atRegex.Matches(template);

            foreach (Match m in matches)
            {
                var type = m.Groups["type"].Success ? m.Groups["type"].Value : "s";
                var fieldName = m.Groups["name"].Value;
                var choice = m.Groups["choice"].Success ? m.Groups["choice"].Value : "";

                DataField field = DataField.GetField(fieldName, m.Value, type, choice);
                flwFields.Controls.Add(field);
            }

            if (_storage.GroupExist(_groupName))
            {
                _storage.LoadCollection(_groupName, flwFields.Controls);
            }
        }
        #endregion

        #region [ Return the Result ]
        public string GetResult()
        {
            string result = lblTemplate.Text;

            foreach (DataField c in flwFields.Controls)
            {
                if (c.IsValid())
                {
                    result = result.Replace(c.ReplacementString, c.Text);
                }
            }
            return result;
        }
        #endregion

        #region [ Ok Press / Save Input ]
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (chkRemember.Checked)
            {
                _storage.SaveCollection(_groupName, flwFields.Controls);
                _storage.SaveToFile();
            }
            DialogResult = DialogResult.OK;
        }
        #endregion

        #region [ Helper ]
        private void FrmInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                foreach (DataField c in flwFields.Controls)
                {
                    if (!c.IsValid())
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void SetToolTip(object sender, EventArgs e)
        {
            var c = (Control)sender;
            toolTip.SetToolTip(c, c.Text);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            foreach (var c in flwFields.Controls)
            {
                ((DataField)c).Clear();
            }
        }
        #endregion
    }
}
