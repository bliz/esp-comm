﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LittleUmph;

namespace Esp_Comm.DataTypes
{
    class StrField: DataField
    {
        public StrField(string fieldName)
            : base(fieldName)
        {
            base.FieldType = DataTypes.FieldType.String;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // cboChoice
            // 
            this.cboChoice.Size = new System.Drawing.Size(254, 30);
            // 
            // StrField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.Name = "StrField";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
