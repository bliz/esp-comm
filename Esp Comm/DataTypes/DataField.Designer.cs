﻿namespace Esp_Comm.DataTypes
{
    partial class DataField
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFieldName = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.lblErrorMsg = new System.Windows.Forms.Label();
            this.cboChoice = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblFieldName
            // 
            this.lblFieldName.AutoSize = true;
            this.lblFieldName.Location = new System.Drawing.Point(5, 3);
            this.lblFieldName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFieldName.Name = "lblFieldName";
            this.lblFieldName.Size = new System.Drawing.Size(110, 22);
            this.lblFieldName.TabIndex = 0;
            this.lblFieldName.Text = "Field Name";
            // 
            // txtValue
            // 
            this.txtValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValue.Location = new System.Drawing.Point(3, 28);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(251, 30);
            this.txtValue.TabIndex = 1;
            // 
            // lblErrorMsg
            // 
            this.lblErrorMsg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblErrorMsg.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorMsg.ForeColor = System.Drawing.Color.Crimson;
            this.lblErrorMsg.Location = new System.Drawing.Point(0, 61);
            this.lblErrorMsg.Name = "lblErrorMsg";
            this.lblErrorMsg.Size = new System.Drawing.Size(254, 27);
            this.lblErrorMsg.TabIndex = 2;
            this.lblErrorMsg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboChoice
            // 
            this.cboChoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboChoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboChoice.FormattingEnabled = true;
            this.cboChoice.Location = new System.Drawing.Point(0, 28);
            this.cboChoice.Name = "cboChoice";
            this.cboChoice.Size = new System.Drawing.Size(254, 30);
            this.cboChoice.TabIndex = 3;
            // 
            // DataField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cboChoice);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.lblFieldName);
            this.Controls.Add(this.lblErrorMsg);
            this.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.Name = "DataField";
            this.Size = new System.Drawing.Size(254, 88);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.TextBox txtValue;
        protected System.Windows.Forms.ComboBox cboChoice;
        protected System.Windows.Forms.Label lblFieldName;
        protected System.Windows.Forms.Label lblErrorMsg;
    }
}
