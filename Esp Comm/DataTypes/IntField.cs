﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LittleUmph;

namespace Esp_Comm.DataTypes
{
    public class IntField : DataField
    {
        public IntField(string fieldName): base(fieldName)
        {
            base.FieldType = DataTypes.FieldType.Int;            
        }
        public override bool IsValid()
        {
            if (base.IsValid())
            {
                var valid = Str.IsInteger(Text);
                if (!valid)
                {
                    base.lblErrorMsg.Text = "This sucker has to be a number.";
                }
                return valid;
            } 
            return false;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // cboChoice
            // 
            this.cboChoice.Size = new System.Drawing.Size(254, 30);
            // 
            // IntField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.Name = "IntField";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
