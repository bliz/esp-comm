﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LittleUmph;

namespace Esp_Comm.DataTypes
{
    /// <summary>
    /// Type of field
    /// </summary>
    public enum FieldType {
        String,
        Int,
        Decimal,
        Choice,
        Byte
    }

    [Serializable]
    public partial class DataField : UserControl
    {

        #region [ Get DataField ]
        /// <summary>
        /// Gets the field base the criteria.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="replacementString">The template.</param>
        /// <param name="typeIndicator">The type indicator.</param>
        /// <param name="choiceString">The choice string.</param>
        /// <returns></returns>
        public static DataField GetField(string fieldName, string replacementString, string typeIndicator, string choiceString)
        {
            DataField field;

            if (typeIndicator == "i")
            {
                field = new IntField(fieldName);
            }
            else if (typeIndicator == "d")
            {
                field = new IntField(fieldName);

            }
            else if (typeIndicator == "c")
            {
                field = new ChoiceField(fieldName, choiceString);
            }
            else
            {
                field = new StrField(fieldName);
            }

            field.ReplacementString = replacementString;

            field.Name = Str.CleanVarName(fieldName);
            return field;
        }
        #endregion

        #region [ Properties ]
        private string _fieldName = "";
        /// <summary>
        /// Gets or sets the name of the field.
        /// </summary>
        /// <value>
        /// The name of the field.
        /// </value>
        public string FieldName
        {
            get
            {
                return _fieldName;
            }
            set
            {
                _fieldName = value;
                lblFieldName.Text = value;
            }
        }
        /// <summary>
        /// Gets or sets the type of the field.
        /// </summary>
        /// <value>
        /// The type of the field.
        /// </value>
        public FieldType FieldType { get; protected set; }

        /// <summary>
        /// The origin string from the variable string (use this to replace value with the template)
        /// </summary>
        /// <value>
        /// The replacement string.
        /// </value>
        public string ReplacementString { get; set; }
        #endregion

        #region [ Override Fields ]
        public override string Text
        {
            get
            {
                return txtValue.Text;
            }
            set
            {
                txtValue.Text = value;
            }
        }
        #endregion

        #region [ Constructors ]
        public DataField()
            : this("DataField")
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataField"/> class.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        public DataField(string fieldName)
        {
            InitializeComponent();

            FieldName = fieldName;
            cboChoice.Hide();
        }
        #endregion

        #region [ Abstract Functions ]
        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsValid()
        {
            if (Str.IsEmpty(Text))
            {
                lblErrorMsg.Text = "This sucker cannot be empty.";
                return false;
            }
            lblErrorMsg.Text = "";
            return true;
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            txtValue.Clear();
            cboChoice.SelectedIndex = -1;
        }
        #endregion
    }
}
