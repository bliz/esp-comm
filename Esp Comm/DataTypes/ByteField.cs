﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LittleUmph;

namespace Esp_Comm.DataTypes
{
    public class ByteField : DataField
    {
        public ByteField(string fieldName) : base(fieldName)
        {
            FieldType = DataTypes.FieldType.Byte;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // cboChoice
            // 
            this.cboChoice.Size = new System.Drawing.Size(254, 30);
            // 
            // ByteField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.Name = "ByteField";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
