﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esp_Comm.DataTypes
{
    class ChoiceField : DataField
    {
        // key = the display text, value = the actual value, 
        Dictionary<string, string> _value = new Dictionary<string, string>();

        public override string Text
        {
            get
            {
                var txt = cboChoice.Text;
                if (_value.ContainsKey(txt))
                {
                    return _value[txt];
                }
                return txt;
            }
            set
            {
                var choiceValue = value;
                if (_value.ContainsValue(choiceValue))
                {    
                    foreach(var pair in _value){
                        if (pair.Value == choiceValue){
                            int index = cboChoice.FindStringExact(pair.Key);
                            cboChoice.SelectedIndex = index;
                        }
                    }
                }
            }
        }

        public ChoiceField(string fieldName, string choiceString)
            : base(fieldName)
        {
            base.FieldType = DataTypes.FieldType.Choice;
            base.cboChoice.Show();

            string[] choices = choiceString.TrimStart(' ', ',').Split('|');
            foreach (var c in choices)
            {
                var s = c.Split('=');
                if (s.Length == 2)
                {
                    _value[s[1]] = s[0];
                    base.cboChoice.Items.Add(s[1]);
                }
                else
                {
                    base.cboChoice.Items.Add(c);
                }
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // cboChoice
            // 
            this.cboChoice.Size = new System.Drawing.Size(254, 30);
            // 
            // ChoiceField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.Name = "ChoiceField";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

    }
}
